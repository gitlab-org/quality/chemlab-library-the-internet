# frozen_string_literal: true

require_relative '../lib/the_internet'

Chemlab.configure do |chemlab|
  chemlab.browser = :chrome, { headless: true }
  chemlab.base_url = 'https://the-internet.herokuapp.com'
end
