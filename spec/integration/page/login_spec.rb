# frozen_string_literal: true

require 'integration_helper'

module TheInternet
  RSpec.describe 'Login' do
    before do
      Page::Login.perform(&:visit)
    end

    context 'with incorrect credentials' do
      let(:username) { 'incorrect' }
      let(:password) { 'incorrect' }

      before do
        Page::Login.perform do |login|
          login.username = username
          login.password = password
          login.login
        end
      end

      it 'shows a flash message' do
        Component::Flash.perform do |flash|
          expect(flash).to be_visible
        end
      end
    end

    context 'with correct credentials' do
      let(:username) { 'tomsmith' }
      let(:password) { 'SuperSecretPassword!' }

      before do
        Page::Login.perform do |login|
          login.username = username
          login.password = password
          login.login
        end
      end

      it 'logs in correctly' do
        Page::Secure.perform do |secure|
          expect(secure).to be_visible
        end
      end
    end
  end
end
