# frozen_string_literal: true

require 'integration_helper'

module TheInternet
  module Page
    RSpec.describe 'Index' do
      before do
        Page::Index.perform(&:visit)
      end

      it 'has all elements upon navigation' do
        Page::Index.perform do |index|
          expect(index).to be_visible
        end
      end
    end
  end
end
