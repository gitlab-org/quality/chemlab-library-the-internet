# frozen_string_literal: true

require 'unit_helper'

module TheInternet
  module Page
    RSpec.describe Secure do
      describe '.path' do
        it 'has the correct path' do
          expect(described_class.path).to eq('/secure')
        end
      end
    end
  end
end
