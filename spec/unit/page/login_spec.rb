# frozen_string_literal: true

require 'unit_helper'

module TheInternet
  module Page
    RSpec.describe Login do
      describe '.path' do
        it 'has the correct path' do
          expect(described_class.path).to eq('/login')
        end
      end
    end
  end
end
