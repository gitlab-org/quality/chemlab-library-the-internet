# frozen_string_literal: true

require 'unit_helper'

module TheInternet
  module Component
    RSpec.describe Flash do
      describe 'elements' do
        it { is_expected.to respond_to(:message) }
        it { is_expected.to respond_to(:close) }
      end
    end
  end
end
