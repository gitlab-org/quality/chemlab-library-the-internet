# frozen_string_literal: true

lib = File.expand_path('lib', __dir__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)

Gem::Specification.new do |spec|
  spec.name = 'chemlab-library-the-internet'
  spec.version = '1.1.0'
  spec.authors = ['GitLab Quality']
  spec.email = ['quality@gitlab.com']

  spec.required_ruby_version = '>= 2.7.2'

  spec.summary = 'Chemlab Library for "The Internet"'
  spec.homepage = 'https://gitlab.com/gitlab-org/quality/chemlab-library-the-internet'
  spec.license = 'MIT'

  spec.files = `git ls-files -- lib/*`.split("\n")

  spec.require_paths = ['lib']

  spec.add_development_dependency 'rspec', '~> 3.7'
  spec.add_development_dependency 'rubocop', '~> 1.14.0'
  spec.add_development_dependency 'rubocop-rspec', '~> 2.3.0'
  spec.add_development_dependency 'yard', '~> 0.9.26'

  spec.add_runtime_dependency 'chemlab', '~> 0.7.3'
end
