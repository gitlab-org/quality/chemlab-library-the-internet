# frozen_string_literal: true

module TheInternet
  module Page
    module Login

      # @note Defined as +text_field :username+
      # @return [String] The text content or value of +username+
      def username
        # This is a stub, used for indexing
      end


      # Set the value of username
      # @example
      #   TheInternet::Page::Login.perform do |login|
      #     login.username = 'value'
      #   end
      # @param value [String] The value to set.
      def username=(value)
        # This is a stub, used for indexing
      end

      # @example
      #   TheInternet::Page::Login.perform do |login|
      #     expect(login.username_element).to exist
      #   end
      # @return [Watir::TextField] The raw +TextField+ element
      def username_element
        # This is a stub, used for indexing
      end

      # @example
      #   TheInternet::Page::Login.perform do |login|
      #     expect(login).to be_username
      #   end
      # @return [Boolean] true if the +username+ element is present on the page
      def username?
        # This is a stub, used for indexing
      end


      # @note Defined as +text_field :password+
      # @return [String] The text content or value of +password+
      def password
        # This is a stub, used for indexing
      end


      # Set the value of password
      # @example
      #   TheInternet::Page::Login.perform do |login|
      #     login.password = 'value'
      #   end
      # @param value [String] The value to set.
      def password=(value)
        # This is a stub, used for indexing
      end

      # @example
      #   TheInternet::Page::Login.perform do |login|
      #     expect(login.password_element).to exist
      #   end
      # @return [Watir::TextField] The raw +TextField+ element
      def password_element
        # This is a stub, used for indexing
      end

      # @example
      #   TheInternet::Page::Login.perform do |login|
      #     expect(login).to be_password
      #   end
      # @return [Boolean] true if the +password+ element is present on the page
      def password?
        # This is a stub, used for indexing
      end


      # @note Defined as +button :login+
      # Clicks +login+
      def login
        # This is a stub, used for indexing
      end



      # @example
      #   TheInternet::Page::Login.perform do |login|
      #     expect(login.login_element).to exist
      #   end
      # @return [Watir::Button] The raw +Button+ element
      def login_element
        # This is a stub, used for indexing
      end

      # @example
      #   TheInternet::Page::Login.perform do |login|
      #     expect(login).to be_login
      #   end
      # @return [Boolean] true if the +login+ element is present on the page
      def login?
        # This is a stub, used for indexing
      end

    end
  end
end