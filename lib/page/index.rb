# frozen_string_literal: true

module TheInternet
  module Page
    # Index page at https://the-internet.herokuapp.com/
    class Index < Chemlab::Page
      path '/'

      h1 :heading, class: 'heading'

      link :a_b_testing, href: '/abtest'
      link :add_remove_elements, href: '/add_remove_elements/'
      link :basic_auth, href: '/basic_auth'
      link :broken_images, href: '/broken_images'
      link :challenging_dom, href: '/challenging_dom'
      link :checkboxes, href: '/checkboxes'
      link :context_menu, href: '/context_menu'
      link :digest_authentication, href: '/digest_auth'
      link :disappearing_elements, href: '/disappearing_elements'
      link :drag_and_drop, href: '/drag_and_drop'
      link :dropdown, href: '/dropdown'
      link :dynamic_content, href: '/dynamic_content'
      link :dynamic_controls, href: '/dynamic_controls'
      link :dynamic_loading, href: '/dynamic_loading'
      link :entry_ad, href: '/entry_ad'
      link :exit_intent, href: '/exit_intent'
      link :file_download, href: '/download'
      link :file_upload, href: '/upload'
      link :floating_menu, href: '/floating_menu'
      link :forgot_password, href: '/forgot_password'
      link :form_authentication, href: '/login'
      link :frames, href: '/frames'
      link :geolocation, href: '/geolocation'
      link :horizontal_slider, href: '/horizontal_slider'
      link :hovers, href: '/hovers'
      link :infinite_scroll, href: '/infinite_scroll'
      link :inputs, href: '/inputs'
      link :jquery_ui_menus, href: '/jqueryui/menu'
      link :javascript_alerts, href: '/javascript_alerts'
      link :javascript_onload_event_error, href: '/javascript_error'
      link :key_presses, href: '/key_presses'
      link :large_and_deep_dom, href: '/large'
      link :multiple_windows, href: '/windows'
      link :nested_frames, href: '/nested_frames'
      link :notification_messages, href: '/notification_message'
      link :redirect_link, href: '/redirector'
      link :secure_file_download, href: '/download_secure'
      link :shadow_dom, href: '/shadowdom'
      link :shifting_content, href: '/shifting_content'
      link :slow_resources, href: '/slow'
      link :sortable_data_tables, href: '/tables'
      link :status_codes, href: '/status_codes'
      link :typos, href: '/typos'
      link :wysiwyg_editor, href: '/tinymce'
    end
  end
end
