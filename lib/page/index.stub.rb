# frozen_string_literal: true

module TheInternet
  module Page
    module Index

      # @note Defined as +h1 :heading+
      # @return [String] The text content or value of +heading+
      def heading
        # This is a stub, used for indexing
      end



      # @example
      #   TheInternet::Page::Index.perform do |index|
      #     expect(index.heading_element).to exist
      #   end
      # @return [Watir::H1] The raw +H1+ element
      def heading_element
        # This is a stub, used for indexing
      end

      # @example
      #   TheInternet::Page::Index.perform do |index|
      #     expect(index).to be_heading
      #   end
      # @return [Boolean] true if the +heading+ element is present on the page
      def heading?
        # This is a stub, used for indexing
      end


      # @note Defined as +link :a_b_testing+
      # Clicks +a_b_testing+
      def a_b_testing
        # This is a stub, used for indexing
      end



      # @example
      #   TheInternet::Page::Index.perform do |index|
      #     expect(index.a_b_testing_element).to exist
      #   end
      # @return [Watir::Link] The raw +Link+ element
      def a_b_testing_element
        # This is a stub, used for indexing
      end

      # @example
      #   TheInternet::Page::Index.perform do |index|
      #     expect(index).to be_a_b_testing
      #   end
      # @return [Boolean] true if the +a_b_testing+ element is present on the page
      def a_b_testing?
        # This is a stub, used for indexing
      end


      # @note Defined as +link :add_remove_elements+
      # Clicks +add_remove_elements+
      def add_remove_elements
        # This is a stub, used for indexing
      end



      # @example
      #   TheInternet::Page::Index.perform do |index|
      #     expect(index.add_remove_elements_element).to exist
      #   end
      # @return [Watir::Link] The raw +Link+ element
      def add_remove_elements_element
        # This is a stub, used for indexing
      end

      # @example
      #   TheInternet::Page::Index.perform do |index|
      #     expect(index).to be_add_remove_elements
      #   end
      # @return [Boolean] true if the +add_remove_elements+ element is present on the page
      def add_remove_elements?
        # This is a stub, used for indexing
      end


      # @note Defined as +link :basic_auth+
      # Clicks +basic_auth+
      def basic_auth
        # This is a stub, used for indexing
      end



      # @example
      #   TheInternet::Page::Index.perform do |index|
      #     expect(index.basic_auth_element).to exist
      #   end
      # @return [Watir::Link] The raw +Link+ element
      def basic_auth_element
        # This is a stub, used for indexing
      end

      # @example
      #   TheInternet::Page::Index.perform do |index|
      #     expect(index).to be_basic_auth
      #   end
      # @return [Boolean] true if the +basic_auth+ element is present on the page
      def basic_auth?
        # This is a stub, used for indexing
      end


      # @note Defined as +link :broken_images+
      # Clicks +broken_images+
      def broken_images
        # This is a stub, used for indexing
      end



      # @example
      #   TheInternet::Page::Index.perform do |index|
      #     expect(index.broken_images_element).to exist
      #   end
      # @return [Watir::Link] The raw +Link+ element
      def broken_images_element
        # This is a stub, used for indexing
      end

      # @example
      #   TheInternet::Page::Index.perform do |index|
      #     expect(index).to be_broken_images
      #   end
      # @return [Boolean] true if the +broken_images+ element is present on the page
      def broken_images?
        # This is a stub, used for indexing
      end


      # @note Defined as +link :challenging_dom+
      # Clicks +challenging_dom+
      def challenging_dom
        # This is a stub, used for indexing
      end



      # @example
      #   TheInternet::Page::Index.perform do |index|
      #     expect(index.challenging_dom_element).to exist
      #   end
      # @return [Watir::Link] The raw +Link+ element
      def challenging_dom_element
        # This is a stub, used for indexing
      end

      # @example
      #   TheInternet::Page::Index.perform do |index|
      #     expect(index).to be_challenging_dom
      #   end
      # @return [Boolean] true if the +challenging_dom+ element is present on the page
      def challenging_dom?
        # This is a stub, used for indexing
      end


      # @note Defined as +link :checkboxes+
      # Clicks +checkboxes+
      def checkboxes
        # This is a stub, used for indexing
      end



      # @example
      #   TheInternet::Page::Index.perform do |index|
      #     expect(index.checkboxes_element).to exist
      #   end
      # @return [Watir::Link] The raw +Link+ element
      def checkboxes_element
        # This is a stub, used for indexing
      end

      # @example
      #   TheInternet::Page::Index.perform do |index|
      #     expect(index).to be_checkboxes
      #   end
      # @return [Boolean] true if the +checkboxes+ element is present on the page
      def checkboxes?
        # This is a stub, used for indexing
      end


      # @note Defined as +link :context_menu+
      # Clicks +context_menu+
      def context_menu
        # This is a stub, used for indexing
      end



      # @example
      #   TheInternet::Page::Index.perform do |index|
      #     expect(index.context_menu_element).to exist
      #   end
      # @return [Watir::Link] The raw +Link+ element
      def context_menu_element
        # This is a stub, used for indexing
      end

      # @example
      #   TheInternet::Page::Index.perform do |index|
      #     expect(index).to be_context_menu
      #   end
      # @return [Boolean] true if the +context_menu+ element is present on the page
      def context_menu?
        # This is a stub, used for indexing
      end


      # @note Defined as +link :digest_authentication+
      # Clicks +digest_authentication+
      def digest_authentication
        # This is a stub, used for indexing
      end



      # @example
      #   TheInternet::Page::Index.perform do |index|
      #     expect(index.digest_authentication_element).to exist
      #   end
      # @return [Watir::Link] The raw +Link+ element
      def digest_authentication_element
        # This is a stub, used for indexing
      end

      # @example
      #   TheInternet::Page::Index.perform do |index|
      #     expect(index).to be_digest_authentication
      #   end
      # @return [Boolean] true if the +digest_authentication+ element is present on the page
      def digest_authentication?
        # This is a stub, used for indexing
      end


      # @note Defined as +link :disappearing_elements+
      # Clicks +disappearing_elements+
      def disappearing_elements
        # This is a stub, used for indexing
      end



      # @example
      #   TheInternet::Page::Index.perform do |index|
      #     expect(index.disappearing_elements_element).to exist
      #   end
      # @return [Watir::Link] The raw +Link+ element
      def disappearing_elements_element
        # This is a stub, used for indexing
      end

      # @example
      #   TheInternet::Page::Index.perform do |index|
      #     expect(index).to be_disappearing_elements
      #   end
      # @return [Boolean] true if the +disappearing_elements+ element is present on the page
      def disappearing_elements?
        # This is a stub, used for indexing
      end


      # @note Defined as +link :drag_and_drop+
      # Clicks +drag_and_drop+
      def drag_and_drop
        # This is a stub, used for indexing
      end



      # @example
      #   TheInternet::Page::Index.perform do |index|
      #     expect(index.drag_and_drop_element).to exist
      #   end
      # @return [Watir::Link] The raw +Link+ element
      def drag_and_drop_element
        # This is a stub, used for indexing
      end

      # @example
      #   TheInternet::Page::Index.perform do |index|
      #     expect(index).to be_drag_and_drop
      #   end
      # @return [Boolean] true if the +drag_and_drop+ element is present on the page
      def drag_and_drop?
        # This is a stub, used for indexing
      end


      # @note Defined as +link :dropdown+
      # Clicks +dropdown+
      def dropdown
        # This is a stub, used for indexing
      end



      # @example
      #   TheInternet::Page::Index.perform do |index|
      #     expect(index.dropdown_element).to exist
      #   end
      # @return [Watir::Link] The raw +Link+ element
      def dropdown_element
        # This is a stub, used for indexing
      end

      # @example
      #   TheInternet::Page::Index.perform do |index|
      #     expect(index).to be_dropdown
      #   end
      # @return [Boolean] true if the +dropdown+ element is present on the page
      def dropdown?
        # This is a stub, used for indexing
      end


      # @note Defined as +link :dynamic_content+
      # Clicks +dynamic_content+
      def dynamic_content
        # This is a stub, used for indexing
      end



      # @example
      #   TheInternet::Page::Index.perform do |index|
      #     expect(index.dynamic_content_element).to exist
      #   end
      # @return [Watir::Link] The raw +Link+ element
      def dynamic_content_element
        # This is a stub, used for indexing
      end

      # @example
      #   TheInternet::Page::Index.perform do |index|
      #     expect(index).to be_dynamic_content
      #   end
      # @return [Boolean] true if the +dynamic_content+ element is present on the page
      def dynamic_content?
        # This is a stub, used for indexing
      end


      # @note Defined as +link :dynamic_controls+
      # Clicks +dynamic_controls+
      def dynamic_controls
        # This is a stub, used for indexing
      end



      # @example
      #   TheInternet::Page::Index.perform do |index|
      #     expect(index.dynamic_controls_element).to exist
      #   end
      # @return [Watir::Link] The raw +Link+ element
      def dynamic_controls_element
        # This is a stub, used for indexing
      end

      # @example
      #   TheInternet::Page::Index.perform do |index|
      #     expect(index).to be_dynamic_controls
      #   end
      # @return [Boolean] true if the +dynamic_controls+ element is present on the page
      def dynamic_controls?
        # This is a stub, used for indexing
      end


      # @note Defined as +link :dynamic_loading+
      # Clicks +dynamic_loading+
      def dynamic_loading
        # This is a stub, used for indexing
      end



      # @example
      #   TheInternet::Page::Index.perform do |index|
      #     expect(index.dynamic_loading_element).to exist
      #   end
      # @return [Watir::Link] The raw +Link+ element
      def dynamic_loading_element
        # This is a stub, used for indexing
      end

      # @example
      #   TheInternet::Page::Index.perform do |index|
      #     expect(index).to be_dynamic_loading
      #   end
      # @return [Boolean] true if the +dynamic_loading+ element is present on the page
      def dynamic_loading?
        # This is a stub, used for indexing
      end


      # @note Defined as +link :entry_ad+
      # Clicks +entry_ad+
      def entry_ad
        # This is a stub, used for indexing
      end



      # @example
      #   TheInternet::Page::Index.perform do |index|
      #     expect(index.entry_ad_element).to exist
      #   end
      # @return [Watir::Link] The raw +Link+ element
      def entry_ad_element
        # This is a stub, used for indexing
      end

      # @example
      #   TheInternet::Page::Index.perform do |index|
      #     expect(index).to be_entry_ad
      #   end
      # @return [Boolean] true if the +entry_ad+ element is present on the page
      def entry_ad?
        # This is a stub, used for indexing
      end


      # @note Defined as +link :exit_intent+
      # Clicks +exit_intent+
      def exit_intent
        # This is a stub, used for indexing
      end



      # @example
      #   TheInternet::Page::Index.perform do |index|
      #     expect(index.exit_intent_element).to exist
      #   end
      # @return [Watir::Link] The raw +Link+ element
      def exit_intent_element
        # This is a stub, used for indexing
      end

      # @example
      #   TheInternet::Page::Index.perform do |index|
      #     expect(index).to be_exit_intent
      #   end
      # @return [Boolean] true if the +exit_intent+ element is present on the page
      def exit_intent?
        # This is a stub, used for indexing
      end


      # @note Defined as +link :file_download+
      # Clicks +file_download+
      def file_download
        # This is a stub, used for indexing
      end



      # @example
      #   TheInternet::Page::Index.perform do |index|
      #     expect(index.file_download_element).to exist
      #   end
      # @return [Watir::Link] The raw +Link+ element
      def file_download_element
        # This is a stub, used for indexing
      end

      # @example
      #   TheInternet::Page::Index.perform do |index|
      #     expect(index).to be_file_download
      #   end
      # @return [Boolean] true if the +file_download+ element is present on the page
      def file_download?
        # This is a stub, used for indexing
      end


      # @note Defined as +link :file_upload+
      # Clicks +file_upload+
      def file_upload
        # This is a stub, used for indexing
      end



      # @example
      #   TheInternet::Page::Index.perform do |index|
      #     expect(index.file_upload_element).to exist
      #   end
      # @return [Watir::Link] The raw +Link+ element
      def file_upload_element
        # This is a stub, used for indexing
      end

      # @example
      #   TheInternet::Page::Index.perform do |index|
      #     expect(index).to be_file_upload
      #   end
      # @return [Boolean] true if the +file_upload+ element is present on the page
      def file_upload?
        # This is a stub, used for indexing
      end


      # @note Defined as +link :floating_menu+
      # Clicks +floating_menu+
      def floating_menu
        # This is a stub, used for indexing
      end



      # @example
      #   TheInternet::Page::Index.perform do |index|
      #     expect(index.floating_menu_element).to exist
      #   end
      # @return [Watir::Link] The raw +Link+ element
      def floating_menu_element
        # This is a stub, used for indexing
      end

      # @example
      #   TheInternet::Page::Index.perform do |index|
      #     expect(index).to be_floating_menu
      #   end
      # @return [Boolean] true if the +floating_menu+ element is present on the page
      def floating_menu?
        # This is a stub, used for indexing
      end


      # @note Defined as +link :forgot_password+
      # Clicks +forgot_password+
      def forgot_password
        # This is a stub, used for indexing
      end



      # @example
      #   TheInternet::Page::Index.perform do |index|
      #     expect(index.forgot_password_element).to exist
      #   end
      # @return [Watir::Link] The raw +Link+ element
      def forgot_password_element
        # This is a stub, used for indexing
      end

      # @example
      #   TheInternet::Page::Index.perform do |index|
      #     expect(index).to be_forgot_password
      #   end
      # @return [Boolean] true if the +forgot_password+ element is present on the page
      def forgot_password?
        # This is a stub, used for indexing
      end


      # @note Defined as +link :form_authentication+
      # Clicks +form_authentication+
      def form_authentication
        # This is a stub, used for indexing
      end



      # @example
      #   TheInternet::Page::Index.perform do |index|
      #     expect(index.form_authentication_element).to exist
      #   end
      # @return [Watir::Link] The raw +Link+ element
      def form_authentication_element
        # This is a stub, used for indexing
      end

      # @example
      #   TheInternet::Page::Index.perform do |index|
      #     expect(index).to be_form_authentication
      #   end
      # @return [Boolean] true if the +form_authentication+ element is present on the page
      def form_authentication?
        # This is a stub, used for indexing
      end


      # @note Defined as +link :frames+
      # Clicks +frames+
      def frames
        # This is a stub, used for indexing
      end



      # @example
      #   TheInternet::Page::Index.perform do |index|
      #     expect(index.frames_element).to exist
      #   end
      # @return [Watir::Link] The raw +Link+ element
      def frames_element
        # This is a stub, used for indexing
      end

      # @example
      #   TheInternet::Page::Index.perform do |index|
      #     expect(index).to be_frames
      #   end
      # @return [Boolean] true if the +frames+ element is present on the page
      def frames?
        # This is a stub, used for indexing
      end


      # @note Defined as +link :geolocation+
      # Clicks +geolocation+
      def geolocation
        # This is a stub, used for indexing
      end



      # @example
      #   TheInternet::Page::Index.perform do |index|
      #     expect(index.geolocation_element).to exist
      #   end
      # @return [Watir::Link] The raw +Link+ element
      def geolocation_element
        # This is a stub, used for indexing
      end

      # @example
      #   TheInternet::Page::Index.perform do |index|
      #     expect(index).to be_geolocation
      #   end
      # @return [Boolean] true if the +geolocation+ element is present on the page
      def geolocation?
        # This is a stub, used for indexing
      end


      # @note Defined as +link :horizontal_slider+
      # Clicks +horizontal_slider+
      def horizontal_slider
        # This is a stub, used for indexing
      end



      # @example
      #   TheInternet::Page::Index.perform do |index|
      #     expect(index.horizontal_slider_element).to exist
      #   end
      # @return [Watir::Link] The raw +Link+ element
      def horizontal_slider_element
        # This is a stub, used for indexing
      end

      # @example
      #   TheInternet::Page::Index.perform do |index|
      #     expect(index).to be_horizontal_slider
      #   end
      # @return [Boolean] true if the +horizontal_slider+ element is present on the page
      def horizontal_slider?
        # This is a stub, used for indexing
      end


      # @note Defined as +link :hovers+
      # Clicks +hovers+
      def hovers
        # This is a stub, used for indexing
      end



      # @example
      #   TheInternet::Page::Index.perform do |index|
      #     expect(index.hovers_element).to exist
      #   end
      # @return [Watir::Link] The raw +Link+ element
      def hovers_element
        # This is a stub, used for indexing
      end

      # @example
      #   TheInternet::Page::Index.perform do |index|
      #     expect(index).to be_hovers
      #   end
      # @return [Boolean] true if the +hovers+ element is present on the page
      def hovers?
        # This is a stub, used for indexing
      end


      # @note Defined as +link :infinite_scroll+
      # Clicks +infinite_scroll+
      def infinite_scroll
        # This is a stub, used for indexing
      end



      # @example
      #   TheInternet::Page::Index.perform do |index|
      #     expect(index.infinite_scroll_element).to exist
      #   end
      # @return [Watir::Link] The raw +Link+ element
      def infinite_scroll_element
        # This is a stub, used for indexing
      end

      # @example
      #   TheInternet::Page::Index.perform do |index|
      #     expect(index).to be_infinite_scroll
      #   end
      # @return [Boolean] true if the +infinite_scroll+ element is present on the page
      def infinite_scroll?
        # This is a stub, used for indexing
      end


      # @note Defined as +link :inputs+
      # Clicks +inputs+
      def inputs
        # This is a stub, used for indexing
      end



      # @example
      #   TheInternet::Page::Index.perform do |index|
      #     expect(index.inputs_element).to exist
      #   end
      # @return [Watir::Link] The raw +Link+ element
      def inputs_element
        # This is a stub, used for indexing
      end

      # @example
      #   TheInternet::Page::Index.perform do |index|
      #     expect(index).to be_inputs
      #   end
      # @return [Boolean] true if the +inputs+ element is present on the page
      def inputs?
        # This is a stub, used for indexing
      end


      # @note Defined as +link :jquery_ui_menus+
      # Clicks +jquery_ui_menus+
      def jquery_ui_menus
        # This is a stub, used for indexing
      end



      # @example
      #   TheInternet::Page::Index.perform do |index|
      #     expect(index.jquery_ui_menus_element).to exist
      #   end
      # @return [Watir::Link] The raw +Link+ element
      def jquery_ui_menus_element
        # This is a stub, used for indexing
      end

      # @example
      #   TheInternet::Page::Index.perform do |index|
      #     expect(index).to be_jquery_ui_menus
      #   end
      # @return [Boolean] true if the +jquery_ui_menus+ element is present on the page
      def jquery_ui_menus?
        # This is a stub, used for indexing
      end


      # @note Defined as +link :javascript_alerts+
      # Clicks +javascript_alerts+
      def javascript_alerts
        # This is a stub, used for indexing
      end



      # @example
      #   TheInternet::Page::Index.perform do |index|
      #     expect(index.javascript_alerts_element).to exist
      #   end
      # @return [Watir::Link] The raw +Link+ element
      def javascript_alerts_element
        # This is a stub, used for indexing
      end

      # @example
      #   TheInternet::Page::Index.perform do |index|
      #     expect(index).to be_javascript_alerts
      #   end
      # @return [Boolean] true if the +javascript_alerts+ element is present on the page
      def javascript_alerts?
        # This is a stub, used for indexing
      end


      # @note Defined as +link :javascript_onload_event_error+
      # Clicks +javascript_onload_event_error+
      def javascript_onload_event_error
        # This is a stub, used for indexing
      end



      # @example
      #   TheInternet::Page::Index.perform do |index|
      #     expect(index.javascript_onload_event_error_element).to exist
      #   end
      # @return [Watir::Link] The raw +Link+ element
      def javascript_onload_event_error_element
        # This is a stub, used for indexing
      end

      # @example
      #   TheInternet::Page::Index.perform do |index|
      #     expect(index).to be_javascript_onload_event_error
      #   end
      # @return [Boolean] true if the +javascript_onload_event_error+ element is present on the page
      def javascript_onload_event_error?
        # This is a stub, used for indexing
      end


      # @note Defined as +link :key_presses+
      # Clicks +key_presses+
      def key_presses
        # This is a stub, used for indexing
      end



      # @example
      #   TheInternet::Page::Index.perform do |index|
      #     expect(index.key_presses_element).to exist
      #   end
      # @return [Watir::Link] The raw +Link+ element
      def key_presses_element
        # This is a stub, used for indexing
      end

      # @example
      #   TheInternet::Page::Index.perform do |index|
      #     expect(index).to be_key_presses
      #   end
      # @return [Boolean] true if the +key_presses+ element is present on the page
      def key_presses?
        # This is a stub, used for indexing
      end


      # @note Defined as +link :large_and_deep_dom+
      # Clicks +large_and_deep_dom+
      def large_and_deep_dom
        # This is a stub, used for indexing
      end



      # @example
      #   TheInternet::Page::Index.perform do |index|
      #     expect(index.large_and_deep_dom_element).to exist
      #   end
      # @return [Watir::Link] The raw +Link+ element
      def large_and_deep_dom_element
        # This is a stub, used for indexing
      end

      # @example
      #   TheInternet::Page::Index.perform do |index|
      #     expect(index).to be_large_and_deep_dom
      #   end
      # @return [Boolean] true if the +large_and_deep_dom+ element is present on the page
      def large_and_deep_dom?
        # This is a stub, used for indexing
      end


      # @note Defined as +link :multiple_windows+
      # Clicks +multiple_windows+
      def multiple_windows
        # This is a stub, used for indexing
      end



      # @example
      #   TheInternet::Page::Index.perform do |index|
      #     expect(index.multiple_windows_element).to exist
      #   end
      # @return [Watir::Link] The raw +Link+ element
      def multiple_windows_element
        # This is a stub, used for indexing
      end

      # @example
      #   TheInternet::Page::Index.perform do |index|
      #     expect(index).to be_multiple_windows
      #   end
      # @return [Boolean] true if the +multiple_windows+ element is present on the page
      def multiple_windows?
        # This is a stub, used for indexing
      end


      # @note Defined as +link :nested_frames+
      # Clicks +nested_frames+
      def nested_frames
        # This is a stub, used for indexing
      end



      # @example
      #   TheInternet::Page::Index.perform do |index|
      #     expect(index.nested_frames_element).to exist
      #   end
      # @return [Watir::Link] The raw +Link+ element
      def nested_frames_element
        # This is a stub, used for indexing
      end

      # @example
      #   TheInternet::Page::Index.perform do |index|
      #     expect(index).to be_nested_frames
      #   end
      # @return [Boolean] true if the +nested_frames+ element is present on the page
      def nested_frames?
        # This is a stub, used for indexing
      end


      # @note Defined as +link :notification_messages+
      # Clicks +notification_messages+
      def notification_messages
        # This is a stub, used for indexing
      end



      # @example
      #   TheInternet::Page::Index.perform do |index|
      #     expect(index.notification_messages_element).to exist
      #   end
      # @return [Watir::Link] The raw +Link+ element
      def notification_messages_element
        # This is a stub, used for indexing
      end

      # @example
      #   TheInternet::Page::Index.perform do |index|
      #     expect(index).to be_notification_messages
      #   end
      # @return [Boolean] true if the +notification_messages+ element is present on the page
      def notification_messages?
        # This is a stub, used for indexing
      end


      # @note Defined as +link :redirect_link+
      # Clicks +redirect_link+
      def redirect_link
        # This is a stub, used for indexing
      end



      # @example
      #   TheInternet::Page::Index.perform do |index|
      #     expect(index.redirect_link_element).to exist
      #   end
      # @return [Watir::Link] The raw +Link+ element
      def redirect_link_element
        # This is a stub, used for indexing
      end

      # @example
      #   TheInternet::Page::Index.perform do |index|
      #     expect(index).to be_redirect_link
      #   end
      # @return [Boolean] true if the +redirect_link+ element is present on the page
      def redirect_link?
        # This is a stub, used for indexing
      end


      # @note Defined as +link :secure_file_download+
      # Clicks +secure_file_download+
      def secure_file_download
        # This is a stub, used for indexing
      end



      # @example
      #   TheInternet::Page::Index.perform do |index|
      #     expect(index.secure_file_download_element).to exist
      #   end
      # @return [Watir::Link] The raw +Link+ element
      def secure_file_download_element
        # This is a stub, used for indexing
      end

      # @example
      #   TheInternet::Page::Index.perform do |index|
      #     expect(index).to be_secure_file_download
      #   end
      # @return [Boolean] true if the +secure_file_download+ element is present on the page
      def secure_file_download?
        # This is a stub, used for indexing
      end


      # @note Defined as +link :shadow_dom+
      # Clicks +shadow_dom+
      def shadow_dom
        # This is a stub, used for indexing
      end



      # @example
      #   TheInternet::Page::Index.perform do |index|
      #     expect(index.shadow_dom_element).to exist
      #   end
      # @return [Watir::Link] The raw +Link+ element
      def shadow_dom_element
        # This is a stub, used for indexing
      end

      # @example
      #   TheInternet::Page::Index.perform do |index|
      #     expect(index).to be_shadow_dom
      #   end
      # @return [Boolean] true if the +shadow_dom+ element is present on the page
      def shadow_dom?
        # This is a stub, used for indexing
      end


      # @note Defined as +link :shifting_content+
      # Clicks +shifting_content+
      def shifting_content
        # This is a stub, used for indexing
      end



      # @example
      #   TheInternet::Page::Index.perform do |index|
      #     expect(index.shifting_content_element).to exist
      #   end
      # @return [Watir::Link] The raw +Link+ element
      def shifting_content_element
        # This is a stub, used for indexing
      end

      # @example
      #   TheInternet::Page::Index.perform do |index|
      #     expect(index).to be_shifting_content
      #   end
      # @return [Boolean] true if the +shifting_content+ element is present on the page
      def shifting_content?
        # This is a stub, used for indexing
      end


      # @note Defined as +link :slow_resources+
      # Clicks +slow_resources+
      def slow_resources
        # This is a stub, used for indexing
      end



      # @example
      #   TheInternet::Page::Index.perform do |index|
      #     expect(index.slow_resources_element).to exist
      #   end
      # @return [Watir::Link] The raw +Link+ element
      def slow_resources_element
        # This is a stub, used for indexing
      end

      # @example
      #   TheInternet::Page::Index.perform do |index|
      #     expect(index).to be_slow_resources
      #   end
      # @return [Boolean] true if the +slow_resources+ element is present on the page
      def slow_resources?
        # This is a stub, used for indexing
      end


      # @note Defined as +link :sortable_data_tables+
      # Clicks +sortable_data_tables+
      def sortable_data_tables
        # This is a stub, used for indexing
      end



      # @example
      #   TheInternet::Page::Index.perform do |index|
      #     expect(index.sortable_data_tables_element).to exist
      #   end
      # @return [Watir::Link] The raw +Link+ element
      def sortable_data_tables_element
        # This is a stub, used for indexing
      end

      # @example
      #   TheInternet::Page::Index.perform do |index|
      #     expect(index).to be_sortable_data_tables
      #   end
      # @return [Boolean] true if the +sortable_data_tables+ element is present on the page
      def sortable_data_tables?
        # This is a stub, used for indexing
      end


      # @note Defined as +link :status_codes+
      # Clicks +status_codes+
      def status_codes
        # This is a stub, used for indexing
      end



      # @example
      #   TheInternet::Page::Index.perform do |index|
      #     expect(index.status_codes_element).to exist
      #   end
      # @return [Watir::Link] The raw +Link+ element
      def status_codes_element
        # This is a stub, used for indexing
      end

      # @example
      #   TheInternet::Page::Index.perform do |index|
      #     expect(index).to be_status_codes
      #   end
      # @return [Boolean] true if the +status_codes+ element is present on the page
      def status_codes?
        # This is a stub, used for indexing
      end


      # @note Defined as +link :typos+
      # Clicks +typos+
      def typos
        # This is a stub, used for indexing
      end



      # @example
      #   TheInternet::Page::Index.perform do |index|
      #     expect(index.typos_element).to exist
      #   end
      # @return [Watir::Link] The raw +Link+ element
      def typos_element
        # This is a stub, used for indexing
      end

      # @example
      #   TheInternet::Page::Index.perform do |index|
      #     expect(index).to be_typos
      #   end
      # @return [Boolean] true if the +typos+ element is present on the page
      def typos?
        # This is a stub, used for indexing
      end


      # @note Defined as +link :wysiwyg_editor+
      # Clicks +wysiwyg_editor+
      def wysiwyg_editor
        # This is a stub, used for indexing
      end



      # @example
      #   TheInternet::Page::Index.perform do |index|
      #     expect(index.wysiwyg_editor_element).to exist
      #   end
      # @return [Watir::Link] The raw +Link+ element
      def wysiwyg_editor_element
        # This is a stub, used for indexing
      end

      # @example
      #   TheInternet::Page::Index.perform do |index|
      #     expect(index).to be_wysiwyg_editor
      #   end
      # @return [Boolean] true if the +wysiwyg_editor+ element is present on the page
      def wysiwyg_editor?
        # This is a stub, used for indexing
      end

    end
  end
end