# frozen_string_literal: true

require 'chemlab'

# The Internet Chemlab Library
module TheInternet
  module Component
    autoload :Flash, 'component/flash'
  end

  module Page
    autoload :Index, 'page/index'
    autoload :Login, 'page/login'
    autoload :Secure, 'page/secure'
  end
end
